<?php
	// Connect to database
	include("db_connect.php");
	include("comptes.php");
	include("utile.php");
	$request_method = $_SERVER["REQUEST_METHOD"];

	/*Retourne tous les vendeurs*/
	function getVendeurs()
	{
		global $conn;
		$query = "SELECT id_vendeur, nom, prenom, email FROM Vendeur NATURAL JOIN Compte";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Retourne un vendeur selon son id*/
	function getVendeur($id_vendeur=0)
	{
		global $conn;
		$query = "SELECT id_vendeur, nom, prenom, email FROM Vendeur NATURAL JOIN Compte";
		if($id_vendeur != 0)
		{
			$query .= " WHERE id_vendeur=".$id_vendeur." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Ajoute un vendeur
	 *Params
	 *nom : Le nom du vendeur
	 *prenom : Le prénom du vendeur
	 *email : L'e-mail du vendeur
	 *mdp : Le mot de passe du vendeur
	 */
	function AddVendeur()
	{
		global $conn;
		$response = array();
		
		
		if(!empty($_POST['nom']) and !empty($_POST['prenom']) and !empty($_POST['email']) and
		!empty($_POST['mdp'])) {

			$stmt = $conn->prepare("INSERT INTO Compte (nom, prenom, email, mdp) VALUES (?,?,?,?)");

			/* Lecture des marqueurs */
			$mdp = cryptPassword($_POST['mdp']);
			$stmt->bind_param("ssss", $_POST['nom'], $_POST['prenom'] , $_POST['email'], $mdp);

			
			/* Exécution de la requête */
			$stmt->execute();
			
			$stmt = $conn->prepare("SELECT id_compte FROM Compte WHERE email=?");
			$stmt->bind_param("s", $_POST['email']);
			$stmt->execute();
			$stmt->bind_result($id_compte);
			$stmt->fetch();
			$id_compte = intval($id_compte);
			$stmt->close();

			$stmt = $conn->prepare("INSERT INTO Vendeur (id_compte) VALUES (?)");
			$stmt->bind_param("i", $id_compte);

			
			if($stmt->execute()) {
				$response['status'] = "Y";
				header("Location: /connexion");
			} else {
				$response['status'] = "N";
				header("Location: /inscription");
			}
			
		
		} else {
			$response['statuts'] = "Il manque des paramètres";
			header("Location: /inscription");
			
		}

		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Met à jour un vendeur*/
	function updateVendeur()
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);
		$id_vendeur = $_PUT["id"];
		$nom = $_PUT["nom"];
		$prenom = $_PUT["prenom"];
		$email = $_PUT["email"];

		$query="UPDATE Compte NATURAL JOIN vendeur SET nom='".$nom."', prenom='".$prenom."', email='".$email."' WHERE id_vendeur=".$id_vendeur;

		if(mysqli_query($conn, $query))
		{
			session_start();

			$_SESSION['nom'] = $nom;
			$_SESSION['prenom'] = $prenom;
			$_SESSION['email'] = $email;

			$response=array(
				'status' => 1,
				'status_message' =>'Vendeur mis a jour avec succes.'
			);
		}else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de la mise a jour de Vendeur. '. mysqli_error($conn)
			);
		}


		header('Content-Type: application/json');
		echo json_encode($response);
	}

	/*Supprime un vendeur*/
	function deleteVendeur()
	{
		global $conn;

		$_DELETE = array();
		parse_str(file_get_contents('php://input'), $_DELETE);
		$id_vendeur = $_DELETE["id"];
		
		/*On supprime l'image*/
		$query = "SELECT id_compte FROM Compte NaTURAL JOIN Vendeur WHERE id_vendeur=".$id_vendeur;
		$result = mysqli_query($conn, $query);

		$res = array();

		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$res[] = $row;
		}


		/*On supprime le compte, les données liées au compte seront supprimées en cascade*/
		$id_compte = $res[0]['id_compte'];

		$query = "DELETE FROM Compte WHERE id_compte=".$id_compte;

		if(mysqli_query($conn, $query))
		{
			session_start();
			session_destroy();
			
			$response=array(
				'status' => 1,
				'status_message' =>'Vendeur supprime avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression du Vendeur a echoue. '. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);

	}

	switch($request_method)
	{

		case 'GET':
			// Retrive Vendeurs
			if(!empty($_GET["id_vendeur"]))
			{
				$id_vendeur=intval($_GET["id_vendeur"]);
				getVendeur($id_vendeur);
			}
			else
			{
				getVendeurs();
			}
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;

		case 'POST':
			// Ajouter un Vendeur
			AddVendeur();
			break;

		case 'PUT':
			// Modifier un Vendeur

			updateVendeur();
			break;

		case 'DELETE':
			// Supprimer un Vendeur

			deleteVendeur();
			break;

	}
?>
