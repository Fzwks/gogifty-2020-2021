<?php
	// Connect to database
	include("db_connect.php");
	include("utile.php");
	$request_method = $_SERVER["REQUEST_METHOD"];

	/*Retourne tous les magasins*/
	function getMagasins()
	{
		global $conn;
		$query = "SELECT id_magasin, id_entreprise, nom, adresse, ville, zipcode, image_path FROM Magasin";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Retourne un magasin selon son id*/
	function getMagasin($id_magasin=0)
	{
		global $conn;
		$query = "SELECT id_magasin, id_entreprise, nom, adresse, ville, zipcode, image_path FROM Magasin";
		if($id_magasin != 0)
		{
			$query .= " WHERE id_magasin=".$id_magasin." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Retourne tous les magasins d'une entreprise selon son id*/
	function getMagasinsEntreprise($id_entreprise=0)
	{
		global $conn;
		$query = "SELECT id_magasin, id_entreprise,  nom, adresse, ville, zipcode, image_path FROM Magasin";
		if($id_entreprise != 0)
		{
			$query .= " WHERE id_entreprise=".$id_entreprise."";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Ajoute un magasin
	 *Params
	 *id_entreprise : L'id de l'entreprise qui possède le magasin
	 *nom : Le nom du magasin
	 *adresse : L'adresse du magasin
	 *ville : La ville du magasin
	 *zipcode : L'adresse postale du magasin
	 */
	function AddMagasin()
	{
		global $conn;
		$response = array();
		
		
        if(!empty($_POST['id_entreprise']) and !empty($_POST['nom']) and !empty($_POST['adresse'])
            and !empty($_POST['ville']) and !empty($_POST['zipcode'])) {

			$stmt = $conn->prepare("INSERT INTO Magasin (id_entreprise, nom, adresse, ville, zipcode) VALUES (?,?,?,?,?)");

			/* Lecture des marqueurs */
            $id_entreprise = intval($_POST['id_entreprise']);
            $zipcode = intval($_POST['zipcode']);
			$stmt->bind_param("isssi", $id_entreprise, $_POST['nom'] , $_POST['adresse'], $_POST['ville'], $_POST['zipcode']);

			
			/* Exécution de la requête */
			if($stmt->execute()) {
				$response['status'] = "Y";
				
			} else {
				$response['status'] = "N";
			}
			$stmt->close();

			$stmt = $conn->prepare("SELECT id_magasin FROM Magasin WHERE id_entreprise = ? AND nom = ? AND adresse = ? AND ville = ? AND zipcode = ?");
			$stmt->bind_param("isssi", $id_entreprise, $_POST['nom'], $_POST['adresse'], $_POST['ville'], $_POST['zipcode']);
			$stmt->execute();
			$stmt->bind_result($id_magasin);
			$stmt->fetch();
			$id_magasin = intval($id_magasin);

			$stmt->close();

			

			/*Upload de l'image de profil si elle est donnée*/
			if (!empty($_FILES["pp"]["name"])) {
				$target_dir = "imgMagasin/". strval($id_magasin); /*Chemin d'enregistrement*/

				$imageFileType = strtolower(pathinfo($_FILES["pp"]["name"],PATHINFO_EXTENSION));
				/*Chemin jusqu'au fichier*/
				$target_file = $target_dir . "/" . substr(hash("sha256", $_POST['nom']. $_POST['adresse']. $_POST['ville']),0,24 ). ".". $imageFileType;
				
				/*Insertion du chemin dans le compte*/
				$stmt = $conn->prepare("UPDATE Magasin SET image_path=? WHERE id_magasin = ?");
				$stmt->bind_param("si", $target_file, $id_magasin);
				$stmt->execute();
				$stmt->close();

				/*Upload du fichier sur le serveur*/
				if (!file_exists("../" . $target_dir)) {
					mkdir("../" . $target_dir, 0777, true);
				}
				move_uploaded_file($_FILES['pp']['tmp_name'],"../".$target_file);
			}
		
		} else {
			$response['statuts'] = "Il manque des paramètres";
			
		}

		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Met à jour un magasin*/
	function updateMagasin($id_magasin)
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);
		$id_magasin = $_PUT["id_magasin"];
        $nom = $_PUT["nom"];
        $adresse = $_PUT["adresse"];
        $ville = $_PUT["ville"];
		$id_entreprise = intval($_PUT['id_entreprise']);
        $zipcode = intval($_PUT['zipcode']);

		$query="UPDATE magasin SET nom='".$nom."', adresse='".$adresse."', ville='".$ville."', zipcode='".$zipcode."' WHERE id_magasin=".$id_magasin;

		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Magasin mis a jour avec succes.'
			);
		}else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de la mise a jour de Magasin. '. mysqli_error($conn)
			);
		}


		header('Content-Type: application/json');
		echo json_encode($response);
	}

	/*Supprime un magasin*/
	function deleteMagasin($id_magasin)
	{
		global $conn;

		/*On supprime l'image*/
		$query = "SELECT image_path FROM Magasin WHERE id_magasin=".$id_magasin;
		$result = mysqli_query($conn, $query);

		$image_path = array();

		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$image_path[] = $row;
		}

		echo ($image_path[0]['image_path']);
		unlink("../". $image_path[0]['image_path']);



		$query = "DELETE FROM Magasin WHERE id_magasin=".$id_magasin;
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Magasin supprime avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression du Magasin a echoue. '. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	switch($request_method)
	{

		case 'GET':
			// Retrive Magasins
			if(!empty($_GET["id_magasin"]))
			{
				$id_magasin=intval($_GET["id_magasin"]);
				getMagasin($id_magasin);
			}elseif (!empty($_GET["id_entreprise"])) {
				$id_entreprise=intval($_GET["id_entreprise"]);
				getMagasinsEntreprise($id_entreprise);
			}
			else
			{
				getMagasins();
			}
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;

		case 'POST':
			// Ajouter un Magasin
			AddMagasin();
			break;

		case 'PUT':
			// Modifier un Magasin
			$id_magasin = intval($_GET["id_magasin"]);
			updateMagasin($id_magasin);
			break;

		case 'DELETE':
			// Supprimer un Magasin
			$id_magasin = intval($_GET["id_magasin"]);
			deleteMagasin($id_magasin);
			break;

	}
?>
