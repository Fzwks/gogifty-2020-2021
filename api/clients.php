<?php
	// Connect to database
	include("db_connect.php");
	include("comptes.php");
	include("utile.php");
	$request_method = $_SERVER["REQUEST_METHOD"];

	/*Retourne tous les clients*/
	function getClients()
	{
		
		global $conn;
		$query = "SELECT id_client, nom, prenom, email, ville, zipcode, image_profil FROM Client NATURAL JOIN Compte";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Retourne un client selon son id*/
	function getClient($id_client=0)
	{
		global $conn;
		$query = "SELECT id_client, nom, prenom, email, ville, zipcode, image_profil FROM Client NATURAL JOIN Compte";
		if($id_client != 0)
		{
			$query .= " WHERE id_client=".$id_client." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Ajoute un client
	 *Params
	 *nom : Le nom du client
	 *prenom : Le prénom du client
	 *email : L'e-mail du client
	 *mdp : Le mot de passe du client
	 *ville : La ville dans laquelle réside le client
	 *zipcode : L'adresse postale de la ville
	 *pp : La photo de profil, si elle est donnée
	 */
	function AddClient()
	{
		global $conn;
		$response = array();

		/*Utilisé uniquement en cas de debug*/
		#echo json_encode($_POST);
		
		
		if(!empty($_POST['nom']) and !empty($_POST['prenom']) and !empty($_POST['email']) and
		!empty($_POST['mdp']) and !empty($_POST['ville']) and !empty($_POST['zipcode'])) {

			$stmt = $conn->prepare("INSERT INTO Compte (nom, prenom, email, mdp) VALUES (?,?,?,?)");

			/* Lecture des marqueurs */
			$mdp = cryptPassword($_POST['mdp']);
			$stmt->bind_param("ssss", $_POST['nom'], $_POST['prenom'] , $_POST['email'], $mdp);

			
			/* Exécution de la requête */
			$stmt->execute();
			
			$stmt = $conn->prepare("SELECT id_compte FROM Compte WHERE email=?");
			$stmt->bind_param("s", $_POST['email']);
			$stmt->execute();
			$stmt->bind_result($id_compte);
			$stmt->fetch();
			$zipcode = intval($_POST['zipcode']);
			$id_compte = intval($id_compte);
			$stmt->close();



			/*Upload de l'image de profil si elle est donnée*/
			if (!empty($_FILES["pp"]["name"])) {
				$target_dir = "pp/". strval($id_compte); /*Chemin d'enregistrement*/

				$imageFileType = strtolower(pathinfo($_FILES["pp"]["name"],PATHINFO_EXTENSION));
				/*Chemin jusqu'au fichier*/
				$target_file = $target_dir . "/" . substr(hash("sha256", $_POST['email']),0,24 ). ".". $imageFileType;
				
				/*Insertion du chemin dans le compte*/
				$stmt = $conn->prepare("UPDATE Compte SET image_profil=? WHERE id_compte = ?");
				$stmt->bind_param("si", $target_file, $id_compte);
				$stmt->execute();
				$stmt->close();

				/*Upload du fichier sur le serveur*/
				if (!file_exists("../" . $target_dir)) {
					mkdir("../" . $target_dir, 0777, true);
				}
				move_uploaded_file($_FILES['pp']['tmp_name'],"../".$target_file);
			}

			/*Création du compte client*/
			$stmt = $conn->prepare("INSERT INTO Client (id_compte, ville, zipcode) VALUES (?,?,?)");
			$stmt->bind_param("isi", $id_compte, $_POST['ville'], $zipcode);

			
			if($stmt->execute()) {
				$response['status'] = "Y";
				header("Location: /connexion");
				
			} else {
				$response['status'] = "N";
				header("Location: /inscription");
			}
			
		
		} else {
			$response['statuts'] = "Il manque des paramètres";
			header("Location: /inscription");
			
		}

		echo json_encode($response, JSON_PRETTY_PRINT);
		
	}

	/*Met à jour un client*/
	function updateClient()
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);


		$id_client = $_PUT["id"];
		$nom = $_PUT["nom"];
		$prenom = $_PUT["prenom"];
		$email = $_PUT["email"];
        $ville = $_PUT["ville"];
        $zipcode = $_PUT["zipcode"];
		$query="UPDATE Client SET ville='".$ville."', zipcode='".$zipcode."' WHERE id_client=".$id_client;

		if(mysqli_query($conn, $query))
		{

			$query="UPDATE Compte  NATURAL JOIN client SET nom='".$nom."', prenom='".$prenom."' WHERE id_client=".$id_client;

			if(mysqli_query($conn, $query))
			{

				session_start();

				$_SESSION['nom'] = $nom;
				$_SESSION['prenom'] = $prenom;
				$_SESSION['email'] = $email;
				$_SESSION['ville'] = $ville;
				$_SESSION['zipcode'] = $zipcode;

				$response=array(
					'status' => 1,
					'status_message' =>'Client mis a jour avec succes.'
				);
			}else
			{
				$response=array(
					'status' => 0,
					'status_message' =>'Echec de la mise a jour de Client. '. mysqli_error($conn)
				);
			}
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de la mise a jour de Client. '. mysqli_error($conn)
			);

		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	/*Supprime un client*/
	function deleteClient()
	{
		global $conn;

		$_DELETE = array();
		parse_str(file_get_contents('php://input'), $_DELETE);
		$id_client = $_DELETE["id"];


		/*On supprime l'image*/
		$query = "SELECT image_profil, id_compte FROM Compte NaTURAL JOIN Client WHERE id_client=".$id_client;
		$result = mysqli_query($conn, $query);

		$res = array();

		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$res[] = $row;
		}

		if (strcmp($res[0]['image_profil'], "pp/default.png") != 0) {
			unlink("../". $res[0]['image_profil']);
		}


		/*On supprime le compte, les données liées au compte seront supprimées en cascade*/
		$id_compte = $res[0]['id_compte'];

		$query = "DELETE FROM Compte WHERE id_compte=".$id_compte;
		if(mysqli_query($conn, $query))
		{

			session_start();
			session_destroy();
			

			$response=array(
				'status' => 1,
				'status_message' =>'Client supprime avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression du Client a echoue. '. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);

	}

	switch($request_method)
	{

		case 'GET':
			// Retrive Clients
			if(!empty($_GET["id_client"]))
			{
				$id_client=intval($_GET["id_client"]);
				getClient($id_client);
			}
			else
			{
				getClients();
			}
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;

		case 'POST':
			// Ajouter un Client
			AddClient();
			break;

		case 'PUT':
			// Modifier un Client
			updateClient();
			break;

		case 'DELETE':
			// Supprimer un Client

			deleteClient();
			break;

	}
?>
