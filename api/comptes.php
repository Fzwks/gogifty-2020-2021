<?php
	// Connect to database
	include("db_connect.php");

	function getCompte($id_compte=0)
	{
		global $conn;
		$query = "SELECT * FROM Compte";
		if($id_compte != 0)
		{
			$query .= " WHERE id_compte=".$id_compte." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		return $response;
	}

	/*Ajoute un compte
	 *Params
	 *nom : Le nom du compte
	 *prenom : Le prénom du compte
	 *email : L'e-mail du compte
	 *mdp : Le mot de passe du compte
	 */
	function AddCompte() {
		if ( !empty($_POST['nom']) and !empty($_POST['prenom']) and !empty($_POST['email']) and !empty($_POST['mdp'])) {

			include 'utile.php';
			global $conn;
			$nom = $_POST["nom"];
			$prenom = $_POST["prenom"];
			$email = $_POST["email"];
			$mdp = cryptPassword($_POST["mdp"]);
			$query="INSERT INTO Compte( nom, prenom, email, mdp) VALUES( '".$nom."', '".$prenom."', '".$email."', '".$mdp."')";
			if(mysqli_query($conn, $query))
			{
				$query = "SELECT id_compte FROM Compte WHERE email=".$email." LIMIT 1";
				$result = mysqli_query($conn, $query);
				echo mysqli_fetch_array($result);
				return 0;
			}
			else
			{
				return mysqli_error($conn);

			}
			
		} else {
			echo "Il manque des paramètres";
			return -1;		
		}
	}

	/*Met à jour un compte*/
	function updateCompte($id_compte)
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);
    	$id_compte = $_PUT["id_compte"];
        $nom = $_PUT["nom"];
        $nom = $_PUT["prenom"];
        $nom = $_PUT["email"];
        $nom = $_PUT["mdp"];
		$query="UPDATE Compte SET id_compte='".$id_compte."', nom='".$nom."', prenom='".$prenom."', email='".$email."', mdp='".$mdp."' WHERE id_compte=".$id_compte;

		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Compte mis a jour avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de la mise a jour de Compte. '. mysqli_error($conn)
			);

		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	/*Supprime un compte*/
	function deleteCompte($id_compte)
	{
		global $conn;
		$query = "DELETE FROM Compte WHERE id_compte=".$id_compte;
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Compte supprime avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression du Compte a echoue. '. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

?>
