<?php
	// Connect to database
	include("db_connect.php");
	include("utile.php");
	$request_method = $_SERVER["REQUEST_METHOD"];

	/*Retourne tous les programmes*/
	function getProgrammes()
	{
		global $conn;
		$query = "SELECT id_entreprise, nom, description FROM Programme";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Retourne un programme selon son id*/
	function getProgramme($id_programme=0)
	{
		global $conn;
		$query = "SELECT id_entreprise, nom, description FROM Programme";
		if($id_programme != 0)
		{
			$query .= " WHERE id_programme=".$id_programme." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Retourne le programme lié à une entreprise*/
	function getProgrammeEntreprise($id_entreprise=0)
	{
		global $conn;
		$query = "SELECT id_entreprise, nom, description FROM Programme";
		if($id_entreprise != 0)
		{
			$query .= " WHERE id_entreprise=".$id_entreprise." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Ajoute un programme
	 *Params
	 *id_entreprise : L'id de l'entreprise qui possède le programme'
	 *nom : Le nom du programme
	 *description : La description du programme
	 */
	function AddProgramme()
	{
		global $conn;
		$response = array();
		
		
        if(!empty($_POST['id_entreprise']) and !empty($_POST['nom']) and !empty($_POST['description'])) {

			$stmt = $conn->prepare("INSERT INTO Programme (id_entreprise, nom, description) VALUES (?,?,?)");

			/* Lecture des marqueurs */
            $id_entreprise = intval($_POST['id_entreprise']);
			$stmt->bind_param("iss", $id_entreprise, $_POST['nom'] , $_POST['description']);

			
			/* Exécution de la requête */
			if($stmt->execute()) {
				$response['status'] = "Y";
				
			} else {
				$response['status'] = "N";
			}		
		
		} else {
			$response['statuts'] = "Il manque des paramètres";
			
		}

		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Met à jour un programme*/
	function updateProgramme($id_programme)
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);
		$id_programme = $_PUT["id_programme"];
        $nom = $_PUT["nom"];
        $description = $_PUT["description"];
		$id_entreprise = intval($_PUT['id_entreprise']);

		$query="UPDATE programme SET nom='".$nom."', description='".$description."' WHERE id_programme=".$id_programme;

		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Programme mis a jour avec succes.'
			);
		}else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de la mise a jour de Programme. '. mysqli_error($conn)
			);
		}


		header('Content-Type: application/json');
		echo json_encode($response);
	}

	/*Supprime un programme*/
	function deleteProgramme($id_programme)
	{
		global $conn;
		$query = "DELETE FROM Programme WHERE id_programme=".$id_programme;
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Programme supprime avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression du Programme a echoue. '. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	switch($request_method)
	{

		case 'GET':
			// Retrive Programmes
			if(!empty($_GET["id_programme"]))
			{
				$id_programme=intval($_GET["id_programme"]);
				getProgramme($id_programme);
			}elseif (!empty($_GET["id_entreprise"])) {
				$id_entreprise=intval($_GET["id_entreprise"]);
				getProgrammeEntreprise($id_entreprise);
			}
			else
			{
				getProgrammes();
			}
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;

		case 'POST':
			// Ajouter un Programme
			AddProgramme();
			break;

		case 'PUT':
			// Modifier un Programme
			$id_programme = intval($_GET["id_programme"]);
			updateProgramme($id_programme);
			break;

		case 'DELETE':
			// Supprimer un Programme
			$id_programme = intval($_GET["id_programme"]);
			deleteProgramme($id_programme);
			break;

	}
?>
