<?php
	// Connect to database
	include("db_connect.php");
	include("utile.php");
	$request_method = $_SERVER["REQUEST_METHOD"];

	/*Retourne toutes les entreprises*/
	function getEntreprises()
	{
		global $conn;
		$query = "SELECT nom, numero_siret FROM Entreprise";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Retourne une entreprise selon son id*/
	function getEntreprise($id_entreprise=0)
	{
		global $conn;
		$query = "SELECT nom, numero_siret FROM Entreprise";
		if($id_entreprise != 0)
		{
			$query .= " WHERE id_entreprise=".$id_entreprise." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Ajoute une entreprise
	 *Params
	 *id_vendeur : L'id du vendeur qui possède l'entreprise'
	 *nom : Le nom de l'entreprise
	 *numero_siret : Le numéro de siret de l'entreprise
	 */
	function AddEntreprise()
	{
		global $conn;
		$response = array();
		
		
		if(!empty($_POST['id_vendeur']) and !empty($_POST['nom']) and !empty($_POST['numero_siret'])) {

			$stmt = $conn->prepare("INSERT INTO Entreprise (id_vendeur, nom, numero_siret) VALUES (?,?,?)");

			/* Lecture des marqueurs */
			$id_vendeur = intval($_POST['id_vendeur']);
			$stmt->bind_param("iss", $id_vendeur, $_POST['nom'] , $_POST['numero_siret']);

			
			/* Exécution de la requête */
			if($stmt->execute()) {
				$response['status'] = "Y";
				
			} else {
				$response['status'] = "N";
			}		
		
		} else {
			$response['statuts'] = "Il manque des paramètres";
			
		}

		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Met à jour une entreprise*/
	function updateEntreprise($id_entreprise)
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);
		$id_entreprise = $_PUT["id_entreprise"];
		$nom = $_PUT["nom"];
		$numero_siret = $_PUT["numero_siret"];

		$query="UPDATE entreprise SET nom='".$nom."', numero_siret='".$numero_siret."' WHERE id_entreprise=".$id_entreprise;

		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Entreprise mis a jour avec succes.'
			);
		}else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de la mise a jour de Entreprise. '. mysqli_error($conn)
			);
		}


		header('Content-Type: application/json');
		echo json_encode($response);
	}

	/*Supprime une entreprise*/
	function deleteEntreprise($id_entreprise)
	{
		global $conn;
		$query = "DELETE FROM Entreprise WHERE id_entreprise=".$id_entreprise;
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Entreprise supprime avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression du Entreprise a echoue. '. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	switch($request_method)
	{

		case 'GET':
			// Retrive Entreprises
			if(!empty($_GET["id_entreprise"]))
			{
				$id_entreprise=intval($_GET["id_entreprise"]);
				getEntreprise($id_entreprise);
			}
			else
			{
				getEntreprises();
			}
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;

		case 'POST':
			// Ajouter un Entreprise
			AddEntreprise();
			break;

		case 'PUT':
			// Modifier un Entreprise
			$id_entreprise = intval($_GET["id_entreprise"]);
			updateEntreprise($id_entreprise);
			break;

		case 'DELETE':
			// Supprimer un Entreprise
			$id_entreprise = intval($_GET["id_entreprise"]);
			deleteEntreprise($id_entreprise);
			break;

	}
?>
