<?php
	// Connect to database
	include("db_connect.php");
	include("utile.php");

	$request_method = $_SERVER["REQUEST_METHOD"];


    /*Retourne les infos clients selon l'id du client*/
	function getInfos_clientsClient($id_client=0)
	{
		global $conn;
		$query = "SELECT id_client, id_programme, nb_points, premium FROM Infos_clients";
		if($id_client != 0)
		{
			$query .= " WHERE id_client=".$id_client;
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

    /*Retourne les infos clients selon l'id du programme*/
	function getInfos_clientsProgramme($id_programme=0)
	{
		global $conn;
		$query = "SELECT id_client, id_programme, nb_points, premium FROM Infos_clients";
		if($id_programme != 0)
		{
			$query .= " WHERE id_programme=".$id_programme."";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

    /*Abonne un client à un programme de fidélité
	 *Params
	 *id_client : L'id du client qui s'abonne
	 *id_programme : L'id du programme à s'abonner
	 */
	function AddInfos_clients()
	{
		global $conn;
		$response = array();
		
		
        if(!empty($_POST['id_client']) and !empty($_POST['id_programme'])) {

			$stmt = $conn->prepare("INSERT INTO Infos_clients (id_client, id_programme) VALUES (?,?)");

			/* Lecture des marqueurs */
            $id_client = intval($_POST['id_client']);
            $id_programme = intval($_POST['id_programme']);
			$stmt->bind_param("ii", $id_client, $id_programme);

			
			/* Exécution de la requête */
			if($stmt->execute()) {
				$response['status'] = "Y";
				
			} else {
				$response['status'] = "N";
			}		
		
		} else {
			$response['statuts'] = "Il manque des paramètres";
			
		}

		echo json_encode($response, JSON_PRETTY_PRINT);
	}

    /*Met à jour les infos_client*/
	function updateInfos_clientsPremium($id_client, $id_programme)
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);

        $premium = $_PUT["premium"];

		$query="UPDATE infos_clients SET premium='".$premium."' WHERE id_client=".$id_client." AND id_programme=".$id_programme;

		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Infos_clients mis a jour avec succes.'
			);
		}else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de la mise a jour de Infos_clients. '. mysqli_error($conn)
			);
		}


		header('Content-Type: application/json');
		echo json_encode($response);
	}

    /*Désabonne un client d'un programme*/
	function deleteInfos_clients($id_client, $id_programme)
	{
		global $conn;
		$query = "DELETE FROM Infos_clients WHERE id_client=".$id_client." AND id_programme=".$id_programme;
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Infos_clients supprime avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression du Infos_clients a echoue. '. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	switch($request_method)
	{

		case 'GET':
			// Retrive Infos_clientss
			if(!empty($_GET["id_client"]))
			{
				$id_client=intval($_GET["id_client"]);
				getInfos_clientsClient($id_client);
			}elseif (!empty($_GET["id_programme"])) {
				$id_programme=intval($_GET["id_programme"]);
				getInfos_clientsProgramme($id_programme);
			}

			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;

		case 'POST':
			// Ajouter un Infos_clients
			AddInfos_clients();
			break;

		case 'PUT':
			// Modifier un Infos_clients
			$id_client = intval($_GET["id_client"]);
            $id_programme = intval($_GET["id_programme"]);
			updateInfos_clientsPremium($id_client, $id_programme);
			break;

		case 'DELETE':
			// Supprimer un Infos_clients
			$id_client = intval($_GET["id_client"]);
            $id_programme = intval($_GET["id_programme"]);
			deleteInfos_clients($id_client, $id_programme);
			break;

	}
?>
