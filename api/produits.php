<?php
	// Connect to database
	include("db_connect.php");
	include("utile.php");
	$request_method = $_SERVER["REQUEST_METHOD"];

    /*Retourne tous les produits*/ 
	function getProduits()
	{
		global $conn;
		$query = "SELECT id_produit, nom, marque, reference, prix, points FROM Produit";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

    /*Retourne un produit selon son id*/
	function getProduit($id_produit=0)
	{
		global $conn;
		$query = "SELECT id_produit, nom, marque, reference, prix, points FROM Produit";
		if($id_produit != 0)
		{
			$query .= " WHERE id_produit=".$id_produit." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

    /*Retourne les produits liés à une offre*/
    /*PAS IMPLEMENTE DANS LA BDD NE PAS UTILISER*/
	function getProduitsOffre($id_offre=0)
	{
		global $conn;
		$query = "SELECT id_produit, nom, marque, reference, prix, points FROM Produit";
		if($id_offre != 0)
		{
			$query .= " WHERE id_offre=".$id_offre."";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

    /*Ajoute un produit
	 *Params
	 *nom : Le nom du produit
	 *marque : La marque du produit
	 *reference : La référence du produit
	 *prix : Le prix du produit
	 *points : Le nombre de points qu'apporte le produit
	 */
	function AddProduit()
	{
		global $conn;
		$response = array();
		
		
		if(!empty($_POST['nom']) and !empty($_POST['marque']) and 
		!empty($_POST['reference']) and !empty($_POST['prix']) and !empty($_POST['points'])) {

			$nom = $_POST['nom'];
			$marque = $_POST['marque'];
			$reference = $_POST['reference'];
			$prix = intval($_POST['prix']);
            $points = intval($_POST['points']);

            $query = "INSERT INTO Produit ( nom, marque, reference, prix, points";
            $valeurs = ") VALUES ('".$nom."','".$marque."','".$reference."','".$prix."','".$points."')";

            $query.= $valeurs;
            		
			if(mysqli_query($conn, $query)) {
				$response['status'] = "Y";
				
			} else {
				$response['status'] = "N";
			}


			/*On récupère l'ID du produit*/
			$stmt = $conn->prepare("SELECT id_produit FROM Produit WHERE nom = ? AND marque = ? AND reference = ?");
			$stmt->bind_param("sss", $nom, $marque, $reference);
			$stmt->execute();
			$stmt->bind_result($id_produit);
			$stmt->fetch();
			$id_produit = intval($id_produit);

			$stmt->close();

			

			/*Upload de l'image du produit si elle est donnée*/
			if (!empty($_FILES["pp"]["name"])) {
				$target_dir = "imgProduit/". strval($id_produit); /*Chemin d'enregistrement*/

				$imageFileType = strtolower(pathinfo($_FILES["pp"]["name"],PATHINFO_EXTENSION));
				/*Chemin jusqu'au fichier*/
				$target_file = $target_dir . "/" . substr(hash("sha256", $nom. $marque. $reference),0,24 ). ".". $imageFileType;
				
				/*Insertion du chemin dans le compte*/
				$stmt = $conn->prepare("UPDATE Produit SET image_path=? WHERE id_produit = ?");
				$stmt->bind_param("si", $target_file, $id_produit);
				$stmt->execute();
				$stmt->close();

				/*Upload du fichier sur le serveur*/
				if (!file_exists("../" . $target_dir)) {
					mkdir("../" . $target_dir, 0777, true);
				}
				move_uploaded_file($_FILES['pp']['tmp_name'],"../".$target_file);
			}
			
		
		} else {
			$response['statuts'] = "Il manque des paramètres";
			
		}

		echo json_encode($response, JSON_PRETTY_PRINT);
	}

    /*Met à jour un produit*/
	function updateProduit($id_produit)
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);
		$id_produit = $_PUT["id_produit"];
		$nom = $_PUT["nom"];
		$reference = $_PUT["reference"];
		$prix = intval($_PUT['prix']);
        $points = intval($_PUT['points']);

		$query="UPDATE Produit SET nom='".$nom."', reference='".$reference."', prix='".$prix."', points='".$points."' WHERE id_produit=".$id_produit;

		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Produit mis a jour avec succes.'
			);
		}else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de la mise a jour de Produit. '. mysqli_error($conn)
			);
		}


		header('Content-Type: application/json');
		echo json_encode($response);
	}

    /*Supprime un produit*/
	function deleteProduit($id_produit)
	{
		global $conn;

		/*On supprime l'image*/
		$query = "SELECT image_path FROM Produit WHERE id_produit=".$id_produit;
		$result = mysqli_query($conn, $query);

		$image_path = array();

		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$image_path[] = $row;
		}

		unlink("../". $image_path[0]['image_path']);


		$query = "DELETE FROM Produit WHERE id_produit=".$id_produit;
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Produit supprime avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression du Produit a echoue. '. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	switch($request_method)
	{

		case 'GET':
			// Retrive Produits
			if(!empty($_GET["id_produit"]))
			{
				$id_produit=intval($_GET["id_produit"]);
				getProduit($id_produit);
			} elseif (!empty($_GET["id_offre"])) {
				$id_offre=intval($_GET["id_offre"]);
				getProduitsOffre($id_offre);
			}
			else
			{
				getProduits();
			}
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;

		case 'POST':
			// Ajouter un Produit
			AddProduit();
			break;

		case 'PUT':
			// Modifier un Produit
			$id_produit = intval($_GET["id_produit"]);
			updateProduit($id_produit);
			break;

		case 'DELETE':
			// Supprimer un Produit
			$id_produit = intval($_GET["id_produit"]);
			deleteProduit($id_produit);
			break;

	}
?>
