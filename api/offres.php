<?php
	// Connect to database
	include("db_connect.php");
	include("utile.php");
	$request_method = $_SERVER["REQUEST_METHOD"];

	/*Retourne toutes les offres disponibles*/
	function getOffres()
	{
		global $conn;
		$query = "SELECT id_offre, nom, description, date_debut, date_fin, date_recurrence, image_path FROM Offre";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Retourne une offre selon son id*/
	function getOffre($id_offre=0)
	{
		global $conn;
		$query = "SELECT id_offre, nom, description, date_debut, date_fin, date_recurrence, image_path FROM Offre";
		if($id_offre != 0)
		{
			$query .= " WHERE id_offre=".$id_offre." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Retourne les offres selon l'id d'un magasin*/
	function getOffresMagasin($id_magasin=0)
	{
		global $conn;
		$query = "SELECT id_offre, nom, description, date_debut, date_fin, date_recurrence, image_path FROM Offre";
		if($id_magasin != 0)
		{
			$query .= " WHERE id_magasin=".$id_magasin."";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Ajoute une offre
	 *Params
	 *id_magasin : L'id du magasin
	 *nom : Le nom de l'offre
	 *description : Une description de l'offre
	 *ratio : Le ratio des points gagnés pour 1 euro
	 *date_debut : La date à laquelle l'offre commence
	 *date_fin : La date à laquelle l'offre s'arrête
	 *date_recurrence : La date de récurrence
	 */
	function AddOffre()
	{
		global $conn;
		$response = array();
		
		
		if(!empty($_POST['id_magasin']) and !empty($_POST['nom']) and !empty($_POST['description']) and 
		 !empty($_POST['date_debut'])) {

			$id_magasin = intval($_POST['id_magasin']);
			$nom = $_POST['nom'];
			
			$description = $_POST['description'];
			$date_debut = $_POST['date_debut'];

            $query = "INSERT INTO Offre (id_magasin, nom, description, date_debut";
            $valeurs = ") VALUES ('".$id_magasin."','".$nom."','".$description."','".$date_debut."'";

            if (!empty($_POST['date_fin'])) {
                $query .= ",date_fin";
                $valeurs .= ",'".$_POST['date_fin']."'";
            }

            if (!empty($_POST['date_recurrence'])) {
                $query .= ",date_recurrence";
                $valeurs .= ",'".$_POST['date_recurrence']."'";
            }

            $query.= $valeurs .")";
            
			
			if(mysqli_query($conn, $query)) {
				$response['status'] = "Y";
				
			} else {
				$response['status'] = "N";
			}

			/*On récupère l'ID du produit*/
			$stmt = $conn->prepare("SELECT id_offre FROM Offre WHERE nom = ? AND description = ? AND date_debut = ?");
			$stmt->bind_param("sss", $nom, $description, $date_debut);
			$stmt->execute();
			$stmt->bind_result($id_offre);
			$stmt->fetch();
			$id_produit = intval($id_offre);

			$stmt->close();

			

			/*Upload de l'image du produit si elle est donnée*/
			if (!empty($_FILES["pp"]["name"])) {
				$target_dir = "imgOffre/". strval($id_offre); /*Chemin d'enregistrement*/

				$imageFileType = strtolower(pathinfo($_FILES["pp"]["name"],PATHINFO_EXTENSION));
				/*Chemin jusqu'au fichier*/
				$target_file = $target_dir . "/" . substr(hash("sha256", $nom. $description. $date_debut),0,24 ). ".". $imageFileType;
				
				/*Insertion du chemin dans le compte*/
				$stmt = $conn->prepare("UPDATE Offre SET image_path=? WHERE id_offre = ?");
				$stmt->bind_param("si", $target_file, $id_offre);
				$stmt->execute();
				$stmt->close();

				/*Upload du fichier sur le serveur*/
				if (!file_exists("../" . $target_dir)) {
					mkdir("../" . $target_dir, 0777, true);
				}
				move_uploaded_file($_FILES['pp']['tmp_name'],"../".$target_file);
			}
			
		
		} else {
			$response['statuts'] = "Il manque des paramètres";
			
		}

		echo json_encode($response, JSON_PRETTY_PRINT);
	}

	/*Modifie les paramètres de l'offre*/
	function updateOffre($id_offre)
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);
		$id_offre = $_PUT["id_offre"];
		$nom = $_PUT["nom"];
		$description = $_PUT["description"];
		$date_debut = $_PUT["date_debut"];
		$date_fin = $_PUT["date_fin"];
		$date_recurrence = $_PUT["date_recurrence"];

		$query="UPDATE Offre SET nom='".$nom."', description='".$description."', date_debut='".$date_debut."', date_fin='".$date_fin."', date_recurrence='".$date_recurrence."' WHERE id_offre=".$id_offre;

		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Offre mis a jour avec succes.'
			);
		}else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de la mise a jour de Offre. '. mysqli_error($conn)
			);
		}


		header('Content-Type: application/json');
		echo json_encode($response);
	}

	/*Supprime une offre*/
	function deleteOffre($id_offre)
	{
		global $conn;

		/*On supprime l'image*/
		$query = "SELECT image_path FROM Offre WHERE id_offre=".$id_offre;
		$result = mysqli_query($conn, $query);

		$image_path = array();

		while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
		{
			$image_path[] = $row;
		}

		unlink("../". $image_path[0]['image_path']);

		$query = "DELETE FROM Offre WHERE id_offre=".$id_offre;
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Offre supprime avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression du Offre a echoue. '. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}

	switch($request_method)
	{

		case 'GET':
			// Retrive Offres
			if(!empty($_GET["id_offre"]))
			{
				$id_offre=intval($_GET["id_offre"]);
				getOffre($id_offre);
				
			} elseif (!empty($_GET["id_magasin"])) {
				$id_magasin=intval($_GET["id_magasin"]);
				getOffresMagasin($id_magasin);
			}
			else
			{
				getOffres();
			}
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;

		case 'POST':
			// Ajouter un Offre
			AddOffre();
			break;

		case 'PUT':
			// Modifier un Offre
			$id_offre = intval($_GET["id_offre"]);
			updateOffre($id_offre);
			break;

		case 'DELETE':
			// Supprimer un Offre
			$id_offre = intval($_GET["id_offre"]);
			deleteOffre($id_offre);
			break;

	}
?>
