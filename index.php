<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap 4 Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="style.css">
</head>
<body>

<div class="container">

	<div id="row1">
 		<div class="button-container">
    		<span class="button-left"><a href="#1"><img src="/imgSystem/filter.png" class="image-carre-reduite"/></a></span>
    		<span class="button-centre"><a href="#2"><img src="/imgSystem/img_qr.png" class="image-carre-reduite"/></a></span>

            <?php if(isset($_SESSION['email'])): ?>
            
                <span class="button-right"><a href="profil"><img src=<?php echo "/". $_SESSION['image_profil'];?> class="image-cropper" /></a></span>

            <?php else: ?>
                <form action="connexion">
                    <div class="input-group mb-3 input-group-sm">
                        <button type="submit" class="btn btn-primary button-right"> → Connexion </button>
                    </div>
                </form>
            <?php endif; ?>
    		
  		
        </div>
	</div>

    <div class="card-deck">
   
            <div class="card">
                <img class="img-card" src="imgProduit/diorjadore.jpg" alt="DIOR - J'adore" >
                <div class="card-body">
                    <p class="card-text" id="Nom_Produit">DIOR - J'adore</p>
                    <p class="card-text" id="Nom_Magasin">Nocibé</p>
                    <p class="card-text" id="Nom_Ville">Le Mans</p>
                    <p class="card-text" id="Nb_Points">200 points</p>
                    <p class="card-text" id="Date_Fin">Fini le 10/01/2022</p>
                </div>
            </div>
       
       
            <div class="card">
                <img class="img-card" src="imgProduit/channel5.jpg" alt="Lacoste">
                <div class="card-body">
                    <p class="card-text" id="Nom">CHANNEL N°5</p>
                    <p class="card-text" id="Nom_Magasin">Nocibé</p>
                    <p class="card-text" id="Nom_Ville">Le Mans</p>
                    <p class="card-text" id="Nb_Points">200 points</p>
                    <p class="card-text" id="Date_Fin">Fini le 10/03/2022</p>
                </div>
            </div>

    </div>

</div>

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap-switch-button@1.1.0/css/bootstrap-switch-button.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap-switch-button@1.1.0/dist/bootstrap-switch-button.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html> 