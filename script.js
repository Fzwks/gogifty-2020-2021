/*Envoie les modifications faites au profil
 *Affiche le statut de la requête
 */
function EnvoyerModificationsProfil () {
    var type = document.getElementById('type_compte').value;
    var xhr = new XMLHttpRequest();

    xhr.open("PUT", 'api/' + type);

    xhr.onload = function () {
        var message = JSON.parse(xhr.responseText)['status_message'];
        window.alert(message);
      };
      

    /*Permet de fournir le contenu du formulaire au format string*/
    var form = new URLSearchParams(new FormData(document.getElementById("informations"))).toString();


    xhr.send(form);
}


function SupprimerCompte () {
    var type = document.getElementById('type_compte').value;
    var xhr = new XMLHttpRequest();

    xhr.open("DELETE", 'api/' + type);

    xhr.onload = function () {
        var message = JSON.parse(xhr.responseText);
        window.alert(message['status_message']);

        if (message['status'] == 1) {
            window.location.replace("/inscription");
        }
      };
      

    /*Permet de fournir le contenu du formulaire au format string*/
    var form = new URLSearchParams(new FormData(document.getElementById("informations"))).toString();


    xhr.send(form);
}

var bouton = document.getElementById("maj");
bouton.addEventListener("click", EnvoyerModificationsProfil);

var boutonSupp = document.getElementById("supprimer");
boutonSupp.addEventListener("click", SupprimerCompte);