-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 23 fév. 2021 à 14:22
-- Version du serveur :  8.0.21
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `eiah_projet`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id_client` int NOT NULL AUTO_INCREMENT,
  `id_compte` int NOT NULL,
  `ville` varchar(32) NOT NULL,
  `zipcode` int NOT NULL,
  PRIMARY KEY (`id_client`),
  UNIQUE KEY `id_compte` (`id_compte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `client` (`id_compte`,`ville`,`zipcode`) VALUES
(1,'Le Mans', 72000),
(2,'Le Mans', 72000);

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

CREATE TABLE IF NOT EXISTS `compte` (
  `id_compte` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(32) NOT NULL,
  `prenom` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `mdp` varchar(32) NOT NULL,
  `image_profil` varchar(64) CHARACTER SET utf8mb4 NOT NULL DEFAULT 'pp/default.png',
  PRIMARY KEY (`id_compte`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `compte` (`nom`, `prenom`, `email`, `mdp`) VALUES
('edouard', 'jean', 'toto@mail.com', 'titi'),
('benjamin', 'kevin', 'totu@mail.com', 'titi');

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE IF NOT EXISTS `entreprise` (
  `id_entreprise` int NOT NULL AUTO_INCREMENT,
  `id_vendeur` int NOT NULL,
  `nom` varchar(64) NOT NULL,
  `numero_siret` varchar(24) NOT NULL,
  PRIMARY KEY (`id_entreprise`),
  UNIQUE KEY `numero_siret` (`numero_siret`),
  KEY `id_vendeur` (`id_vendeur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `entreprise` (`id_vendeur`, `nom`, `numero_siret`) VALUES
(1, 'jean', 'po5gfg');

-- --------------------------------------------------------

--
-- Structure de la table `infos_clients`
--

CREATE TABLE IF NOT EXISTS `infos_clients` (
  `id_client` int NOT NULL,
  `id_programme` int NOT NULL,
  `nb_points` int NOT NULL DEFAULT '0',
  `premium` varchar(24) NOT NULL DEFAULT 'Aucun',
  UNIQUE KEY `id_client` (`id_client`,`id_programme`),
  KEY `id_programme` (`id_programme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `infos_clients` (`id_client`, `id_programme`, `nb_points`, `premium`) VALUES
(1, 1, 0, 'aucun'),
(2, 1, 0, 'gold');

-- --------------------------------------------------------

--
-- Structure de la table `magasin`
--

CREATE TABLE IF NOT EXISTS `magasin` (
  `id_magasin` int NOT NULL AUTO_INCREMENT,
  `id_entreprise` int NOT NULL,
  `nom` varchar(64) NOT NULL,
  `adresse` varchar(128) NOT NULL,
  `ville` varchar(64) NOT NULL,
  `zipcode` int NOT NULL,
  `image_path` varchar(64) NOT NULL,
  PRIMARY KEY (`id_magasin`),
  UNIQUE KEY `adresse` (`adresse`,`ville`,`zipcode`),
  KEY `id_entreprise` (`id_entreprise`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `magasin` (`id_entreprise`, `nom`, `adresse`, `ville`,`zipcode`,`image_path`) VALUES
(1,'Sephora','15 rue fleurus','Le Mans',72000,'pp/default.png');


-- --------------------------------------------------------

--
-- Structure de la table `offre`
--

CREATE TABLE IF NOT EXISTS `offre` (
  `id_offre` int NOT NULL AUTO_INCREMENT,
  `id_magasin` int NOT NULL,
  `id_produit` int(11) NOT NULL,
  `nom` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `ratio` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  `date_debut` date NOT NULL,
  `date_fin` date DEFAULT NULL,
  `date_recurrence` date DEFAULT NULL,
  `image_path` varchar(64) NOT NULL,
  PRIMARY KEY (`id_offre`),
  KEY `id_magasin` (`id_magasin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `offre` (`id_magasin`, `id_produit`, `nom`, `description`, `ratio`, `quantite`, `date_debut`, `date_fin`, `date_recurrence`,`image_path`) VALUES
(1, 1, 'montre moins 20', 'dfdf', 10, 10, '2021-02-26', '2021-02-28', NULL,'pp/default.png'),
(1, 1, 'montre moins 40', 'fefe', 10, 10, '2021-03-06', '2021-03-13', NULL,'pp/default.png');

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE IF NOT EXISTS `produit` (
  `id_produit` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(32) NOT NULL,
  `marque` varchar(24) NOT NULL,
  `description` text CHARACTER SET utf8mb4,
  `reference` varchar(24) DEFAULT NULL,
  `prix` int NOT NULL,
  `points` int NOT NULL,
  `image_path` varchar(64) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`id_produit`),
  UNIQUE KEY `reference` (`reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `produit` (`nom`, `marque`, `description`,`reference`,`prix`,`points`,`image_path`) VALUES
('montre','Sephora','beau','azer25',100,100,'pp/default.png'),
('collier','Sephora','beau','azerty48',100,100,'pp/default.png');

-- --------------------------------------------------------

--
-- Structure de la table `programme`
--

CREATE TABLE IF NOT EXISTS `programme` (
  `id_programme` int NOT NULL AUTO_INCREMENT,
  `id_entreprise` int NOT NULL,
  `nom` varchar(64) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id_programme`),
  UNIQUE KEY `id_entreprise` (`id_entreprise`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `programme` (`id_entreprise`, `nom`, `description`) VALUES
(1,'beau programme','tres beau');


-- --------------------------------------------------------

--
-- Structure de la table `vendeur`
--

CREATE TABLE IF NOT EXISTS `vendeur` (
  `id_vendeur` int NOT NULL AUTO_INCREMENT,
  `id_compte` int NOT NULL,
  PRIMARY KEY (`id_vendeur`),
  UNIQUE KEY `id_compte` (`id_compte`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `vendeur` (`id_compte`) VALUES
(1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `client_ibfk_1` FOREIGN KEY (`id_compte`) REFERENCES `compte` (`id_compte`) ON DELETE CASCADE;

--
-- Contraintes pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD CONSTRAINT `entreprise_ibfk_1` FOREIGN KEY (`id_vendeur`) REFERENCES `vendeur` (`id_vendeur`) ON DELETE CASCADE;

--
-- Contraintes pour la table `infos_clients`
--
ALTER TABLE `infos_clients`
  ADD CONSTRAINT `infos_clients_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `client` (`id_client`) ON DELETE CASCADE,
  ADD CONSTRAINT `infos_clients_ibfk_2` FOREIGN KEY (`id_programme`) REFERENCES `programme` (`id_programme`) ON DELETE CASCADE;

--
-- Contraintes pour la table `magasin`
--
ALTER TABLE `magasin`
  ADD CONSTRAINT `magasin_ibfk_1` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE CASCADE;

--
-- Contraintes pour la table `offre`
--
ALTER TABLE `offre`
  ADD CONSTRAINT `offre_ibfk_1` FOREIGN KEY (`id_magasin`) REFERENCES `magasin` (`id_magasin`) ON DELETE CASCADE;

--
-- Contraintes pour la table `programme`
--
ALTER TABLE `programme`
  ADD CONSTRAINT `programme_ibfk_1` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE CASCADE;

--
-- Contraintes pour la table `vendeur`
--
ALTER TABLE `vendeur`
  ADD CONSTRAINT `vendeur_ibfk_1` FOREIGN KEY (`id_compte`) REFERENCES `compte` (`id_compte`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;