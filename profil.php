<?php
    session_start();
    if(!isset($_SESSION['email'])){ //if login in session is not set
        header("Location: /connexion");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap 4 Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="style.css">
</head>
<body>

<div class="container">

    <div id="row1">
 		<div class="button-container">
    		<span class="button-left"><a href="index"><img src="imgSystem/filter.png" class="image-carre-reduite"/></a></span>
    		<span class="button-centre"><a href="#2"><img src="imgSystem/img_qr.png" class="image-carre-reduite"/></a></span>
    		<span class="button-right"><a href="profil"><img src=<?php echo $_SESSION['image_profil'];?> class="image-cropper" /></a></span>
  		</div>
	</div>

 

    <form action="api/deconnexion">
        <div class="input-group mb-3 input-group-sm">
            <input type="submit" class="btn btn-secondary" value="Se déconnecter" />
        </div>
    </form>

    <?php if(strcmp($_SESSION['type'], "vendeurs") == 0): ?>
        <div class="input-group mb-3 input-group-sm">
            <a href="/coteVendeur/configurationVendeur" class="badge badge-info">Accéder à la configuration vendeur</a>
        </div>


    <?php endif; ?>

    <div hidden>
            <input type="text" id="type_compte" value=<?php echo $_SESSION['type'];?>>
    </div>


    <!-- Form Profil --> 
    <form id="informations">

        <div hidden>
            <input type="text" class="form-control"  name="id" value=<?php echo $_SESSION['id'];?>>
        </div>


        <!-- Nom -->
        <div class="input-group mb-3 input-group-sm">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                        <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                      </svg>
                </span>
            </div>
            <input type="text" class="form-control" placeholder="nom" name="nom" value=<?php echo $_SESSION['nom'];?>>
        </div>
      
        <!-- Prenom-->
        <div class="input-group mb-3 input-group-sm">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                        <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                      </svg>
                </span>
            </div>
            <input type="text" class="form-control" placeholder="prenom" name="prenom" value=<?php echo $_SESSION['prenom'];?>>
        </div>

        <!-- Email -->
        <div class="input-group mb-3 input-group-sm">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-envelope" viewBox="0 0 16 16">
                        <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
                    </svg>
                </span>
            </div>
            <input type="email" class="form-control" placeholder="email" name="email" value=<?php echo $_SESSION['email'];?>>
        </div>

        <div class="input-group mb-3 input-group-sm">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
                        <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                        <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                      </svg>
                </span>
            </div>
            <input type="text" class="form-control" placeholder="adresse postale" name="zipcode" value=<?php echo $_SESSION['zipcode'];?>>
        </div>

        <div class="input-group mb-3 input-group-sm">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-geo-alt" viewBox="0 0 16 16">
                        <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                        <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                      </svg>
                </span>
            </div>
            <input type="text" class="form-control" placeholder="ville" name="ville" value=<?php echo $_SESSION['ville'];?>>
        </div>

        

    </form>

    <div class="input-group mb-3 input-group-sm">
        <button id="maj"  class="btn btn-success"> Modifier mes informations </button>
    </div>

    <div class="input-group mb-3 input-group-sm">
        <button id="supprimer"  class="btn btn-danger"> Supprimer mon compte </button>
    </div>


</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


<script src="script.js"></script>
</body>
</html> 