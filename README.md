# Gogifty

## Présentation

**Projet Universitaire de R&D à l'Université du Maine. Proposé par Monsieur Madeth May.**

## Installation

1. Téléchargez le repo.
2. Mettez en place un serveur (*Wamp, ...*) et importez la base de données contenue dans le fichier  [base.sql](base.sql).
3. Modifiez le fichier [api/db_connect.php](api/db_connect.php) pour vous connecter au serveur.

## Manuel utilisateur

Le manuel utilisateur est disponible ici : [documentation/manuel_utilisateur/Manuel utilisateur.pdf](documentation/manuel_utilisateur/Manuel utilisateur.pdf)
