<?php
	include('../api/db_connect.php');
 
    session_start();
    if(!isset($_SESSION['email'])){ //if login in session is not set
        header("Location: /connexion");
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Configuration vendeur</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="/style.css">
</head>
<body>

<div class="container">
    <center> <div class="display-4">Gogifty</div> </center>

    <div id="row1">
 		<div class="button-container">
    		<span class="button-left"><a href="/"><img src="/imgSystem/filter.png" class="image-carre-reduite"/></a></span>
    		<span class="button-centre"><a href="#2"><img src="/imgSystem/img_qr.png" class="image-carre-reduite"/></a></span>

            <?php if(isset($_SESSION['email'])): ?>
            
                <span class="button-right"><a href="/profil"><img src=<?php echo "/". $_SESSION['image_profil'];?> class="image-cropper" /></a></span>

            <?php else: ?>
                <form action="connexion">
                    <div class="input-group mb-3 input-group-sm">
                        <button type="submit" class="btn btn-primary button-right"> → Connexion </button>
                    </div>
                </form>
            <?php endif; ?>
    		
  		
        </div>
	</div>
   
   <h6>Les clients </h6> <center>
	<form action='majFidelite.php' method='post'>
		<?php
			$conn -> set_charset("utf8"); 
			// création de la requête
			$requete = "SELECT * FROM client NATURAL JOIN compte NATURAL JOIN infos_clients JOIN programme WHERE id_entreprise=1";
			// envoi de la requête
			$listeClients = $conn->query($requete);
			while($client = $listeClients->fetch_assoc()) { ?>
				<br>
				<?php echo $client['prenom'];?>
				<select value='<?php echo $client['premium']; ?>'>
					<option type=int value="Aucun"  ><?php echo "Aucun" ?> </option>
					<option type=int value="Gold"><?php echo "Gold" ?> </option>
					<option type=int value="Silver"><?php echo "Silver" ?> </option>
					<option type=int value="Ultimate"><?php echo "Ultimate" ?> </option>
				</select>
				</br><br>
			<?php }
		?> </center>
		<div class="input-group mb-3 input-group-sm">
            <button type="submit" class="btn btn-primary"> → Modifier les privilèges </button>
        </div>
	</form>
   
   <h6>Les offres </h6>
   <center>
	<form action='suppression.php' method='post'>
		<?php
			$conn -> set_charset("utf8");
			// création de la requête
			$requete = "SELECT * FROM offre WHERE id_magasin=1";
			// envoi de la requête
			$listeOffres = $conn->query($requete);
			while($offre = $listeOffres->fetch_assoc()) { ?>
				<br>
					<input type='checkbox' name='supp[]' value='<?php echo $offre['id_offre']; ?>' />
					<?php echo $offre['nom'] . ' ' . $offre['date_fin'];
					//$reqTmp = "SELECT `nom` FROM produit WHERE id_produit=".$offre['id_produit_offre']." ";
					//$resultat = $con->query($reqTmp);
					//echo (int) $resultat; //affichage du nom impossible

					?>
				</br>
				<?php }
		?>
	</center>
		<div class="input-group mb-3 input-group-sm">
            <button type="submit" class="btn btn-primary"> → Supprimer le(s) offre(s) </button>
        </div>
	</form>
   
    <h6>Ajouter une offre</h6>

   
    <!-- Form Ajout offre --> 
    <form method="POST" action='ajout.php' enctype="multipart/form-data">

        <!-- ID magasin -->
        <div class="input-group mb-3 input-group-sm">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                        <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                      </svg>
                </span>
            </div>
            <input type="text" class="form-control" placeholder="id_magasin" name="id_magasin">
        </div>

        <!-- Nom -->
        <div class="input-group mb-3 input-group-sm">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                        <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                      </svg>
                </span>
            </div>
            <input type="text" class="form-control" placeholder="nom" name="nom">
        </div>
      
        <!-- Description -->
        <div class="input-group mb-3 input-group-sm">
            <textarea  type="text" class="form-control" placeholder="description" name="description" rows="3"></textarea>
        </div>
        Produit :
		<select name="produit">
			<?php
				$conn -> set_charset("utf8");
				// création de la requête
				$requete = "SELECT * FROM produit";
				// envoi de la requête
				$listeProduits = $conn->query($requete);
				while($produit = $listeProduits->fetch_assoc()) { ?>
						<option type=int value=<?php echo $produit['id_produit']; ?>>
						<?php echo $produit['nom'] ?> </option>
					<?php }
			?>
		</select><br>
		
		<!-- Nb de points requis -->
        <div class="input-group mb-3 input-group-sm">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                        <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                      </svg>
                </span>
            </div>
            <input type="text" class="form-control" placeholder="nombre de points requis" name="ratio">
        </div>
		
		<!-- Quantité -->
        <div class="input-group mb-3 input-group-sm">
            <div class="input-group-prepend">
                <span class="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person" viewBox="0 0 16 16">
                        <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
                      </svg>
                </span>
            </div>
            <input type="text" class="form-control" placeholder="quantité" name="quantite">
        </div>
		
        <!-- Date de début de l'offre-->
        <label for="exampleFormControlTextarea1">Date du début de l'offre</label>
        <div class="input-group mb-3 input-group-sm">
            
            <input type="date" class="form-control"  name="date_debut"> 
        </div>

        <!-- Date de la fin de l'offre-->
        <label for="exampleFormControlTextarea1">Date de la fin de l'offre</label>
        <div class="input-group mb-3 input-group-sm">
            
            <input type="date" class="form-control" placeholder="Date de la fin de l'offre" name="date_fin">
        </div>


        <div class="custom-file input-group mb-3 input-group-sm">
            <input type="file" class="custom-file-input form-control" id="pp" accept=".jpg,.png" name="pp">
            <label class="custom-file-label" for="pp">Choisissez une image pour l'offre</label>
            <script
			  src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
			  integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs="
			  crossorigin="anonymous"></script>
            <script>
                // Add the following code if you want the name of the file appear on select
                $(".custom-file-input").on("change", function() {
                var fileName = $(this).val().split("\\").pop();
                $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                });
            </script>
        </div>
		Date de récurrence :
		<select type="varchar" name="recur">
			<option value='aucun'>aucun</option>
			<option value='lundi'>lundi</option>
			<option value='mardi'>mardi</option>
			<option value='mercredi'>mercredi</option>
			<option value='jeudi'>jeudi</option>
			<option value='vendredi'>vendredi</option>
			<option value='samedi'>samedi</option>
			<option value='dimanche'>dimanche</option>
		</select>

        <div class="input-group mb-3 input-group-sm">
            <button type="submit" class="btn btn-primary"> → Ajouter l'offre </button>
        </div>

        
    </form> 
  
</div>

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap-switch-button@1.1.0/css/bootstrap-switch-button.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap-switch-button@1.1.0/dist/bootstrap-switch-button.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</body>
</html> 